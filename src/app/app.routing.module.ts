import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavBarComponent } from './bar/nav-bar/nav-bar.component';
import { DishInformationComponent } from './dishes/dish-information/dish-information.component';
import { MenuComponent } from './dishes/menu/menu.component';
import { OrderComponent } from './dishes/order/order.component';
import { HomeComponent } from './profile/home/home.component';
import { LoginComponent } from './profile/login/login.component';
import { RegisterComponent } from './profile/register/register.component';
import { UserProfileComponent } from './profile/user-profile/user-profile.component';


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        pathMatch:  'full'
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'profile',
        component: UserProfileComponent,
    },
    {
        path: 'menu',
        component: MenuComponent,
    },
    {
        path: 'order',
        component: OrderComponent,
    },
    {
        path: 'dish-information/:id',
        component: DishInformationComponent,
    }
]


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule{

}