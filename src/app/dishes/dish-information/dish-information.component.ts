import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Dish } from 'src/app/interfaces/dishes.interface';
import { DishesService } from 'src/app/services/dishes.service';
import { IconsService } from 'src/app/services/icons.service';
import { NotificationService } from 'src/app/services/notifications.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-dish-information',
  templateUrl: './dish-information.component.html',
  styleUrls: ['./dish-information.component.css']
})
export class DishInformationComponent implements OnInit {

  id:any;
  dishInformation:Dish = {
    id:0,
    description:"",
    name:"",
    photoUrl:"",
    price:0,
    amount:0,
    totalPrice:0
  };
  imageToShow: any;
  description:string='';

  constructor(private route:ActivatedRoute,
    private dishesService:DishesService,
    private notifications:NotificationService,
    private router:Router,
    public icons:IconsService,
    private orderService:OrderService
  ) { 
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id");
    this.dishesService.getDishInformation(this.id).subscribe( result => {
        if(result.success) {
          this.dishInformation = {
            id: result.data.id,
            description: result.data.description,
            name: result.data.name,
            photoUrl: result.data.photoUrl,
            price: result.data.price,
            amount:1,
            totalPrice:result.data.price,
          };
          this.getImage();
          this.description = this.dishInformation.description;
        } else {
          this.router.navigate(['/menu']);
          this.notifications.showError(result.data, "");
        }
    });
  }

  getImage() {
    this.dishesService.getImage(this.dishInformation.photoUrl).subscribe(data => {
      let reader = new FileReader();
      reader.addEventListener("load", () => {
          this.imageToShow = reader.result;
      }, false);

      if (data) {
          reader.readAsDataURL(data);
      }
      reader.removeEventListener;
      reader.DONE;
      reader.abort;
    }, error => {
      console.log(error);
    });
  }

  changeAmount(amount: number) {
    if(amount == -1) {
      if(this.dishInformation.amount == 1) return;
    }
    this.dishInformation.amount += amount;
    this.dishInformation.totalPrice = this.dishInformation.amount * this.dishInformation.price;
  }

  goToMenu() {
    this.router.navigateByUrl('menu');
  }

  addToBasket() {
    this.orderService.addDishToBasket(this.dishInformation);
    this.goToMenu();
  }
}
