import { Component, OnInit } from '@angular/core';
import { Dish } from 'src/app/interfaces/dishes.interface';
import { OrderService } from 'src/app/services/order.service';
import { IconsService } from '../../services/icons.service';
import { Router } from '@angular/router';
import { DishToOrder } from '../../interfaces/dishes.interface';
import { DishesService } from 'src/app/services/dishes.service';
import { NotificationService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order:DishToOrder[] = [];
  haveOrder:boolean = false;
  totalPrice:number = 0;

  constructor(private orderService:OrderService,
    public icons:IconsService,
    private router:Router,
    private dishesService:DishesService,
    private notifications:NotificationService) { 
    this.setOrder();
  }

  setOrder() {
    this.orderService.getOrder().then( order => {
      this.order = order;
      if(order.length == 0) {
        this.haveOrder =false;
      }else {
        this.haveOrder = true;
        this.totalPrice = 0;
        for (let index = 0; index < this.order.length; index++) {
          this.totalPrice += this.order[index].totalPrice;
        }
      }
    });
  }

  ngOnInit(): void {
  }

  removeDish(id:number) {
    this.orderService.deleteDishFromBasket(id).then(() => {
      this.setOrder();
    });
  }

  sendOrder() {
    this.dishesService.confirmOrder(this.order).subscribe( result => {
      if(result.success) {
        this.router.navigateByUrl('menu');
        this.orderService.deleteOrder().then(() => {
          this.setOrder();
        });
        this.notifications.showSuccess(result.data, "");
      } else {
        this.notifications.showError(result.data, "");
      }
    });
  }

  goToMenu() {
    this.router.navigateByUrl('menu');
  }

}
