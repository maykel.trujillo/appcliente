import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { OrderComponent } from './order/order.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { DishInformationComponent } from './dish-information/dish-information.component';

@NgModule({
  declarations: [
    MenuComponent,
    OrderComponent,
    DishInformationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule
  ],
  exports: [
    MenuComponent,
    OrderComponent,
    DishInformationComponent
  ]
})
export class DishesModule { }
