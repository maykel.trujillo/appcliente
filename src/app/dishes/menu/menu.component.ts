import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DishToList } from 'src/app/interfaces/dishes.interface';
import { DishesService } from 'src/app/services/dishes.service';
import { IconsService } from 'src/app/services/icons.service';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  dishes:DishToList[] = [];
  filterName:string="";
  minPrice:any;
  maxPrice:any;

  constructor(private dishesService: DishesService,
     public icons:IconsService,
     private profileService: ProfileService,
     private router: Router) { 
      profileService.setActualPositionBar('2');
  }

  ngOnInit(): void {
    this.searchDishes();
  }

  searchDishes() {
    this.dishesService.getMenu(this.filterName, this.minPrice, this.maxPrice).subscribe( result => {
      this.dishes = <DishToList[]>result.data;
    });
  }

  dishInformation(id:number) {
    this.router.navigate(['/dish-information', id]);
  }

}
