export interface Profile {
    Id:string;
    Name:string;
    State:string;
    City:string;
    StreetAdress:string;
    Email:string
}

export interface Login {
    Email:string;
    PasswordHash:string;
}

export interface Register {
    Name:string;
    State:string;
    City:string;
    StreetAdress:string;
    Email:string;
    ConfirmEmail:string;
    PasswordHash:string;
    ConfirmPassword:string;
}