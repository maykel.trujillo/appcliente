export interface DishToList {
    id:number;
    name:string;
    price:number;
}

export interface Dish {
    id:number;
    name:string;
    description:string;
    price:number;
    photoUrl:string;
    amount:number;
    totalPrice:number;
}

export interface DishToOrder {
    id:number;
    name:string;
    totalPrice:number;
    amount:number;
}