import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Register } from 'src/app/interfaces/profile.interface';
import { NotificationService } from 'src/app/services/notifications.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
  ]
})
export class RegisterComponent {

  register:Register = {
    Name:"",
    State:"",
    City:"",
    StreetAdress:"",
    Email:"",
    ConfirmEmail:"",
    PasswordHash:"",
    ConfirmPassword:""
  }

  form:FormGroup;

  constructor(private profileService:ProfileService, 
    private router:Router,
    private notifications:NotificationService,
    private formBuilder:FormBuilder) { 
      this.form = this.formBuilder.group({
      });
    }

  
  initRegister() {
    this.form = this.formBuilder.group({
      'name': [this.register.Name, [Validators.required]],
      'state': [this.register.State, [Validators.required]],
      'city': [this.register.City, [Validators.required]],
      'streetAddress': [this.register.StreetAdress, [Validators.required]],
      'email': [this.register.Email, [Validators.required, Validators.email]],
      'confirmEmail': [this.register.ConfirmEmail, [Validators.required, Validators.email]],
      'password': [this.register.PasswordHash, [Validators.required]],
      'confirmPassword': [this.register.ConfirmPassword, [Validators.required]],
    });

    if(this.form.valid) {

      if(this.form.value.email != this.form.value.confirmEmail) {
        this.notifications.showInfo("The email confirmation does not match", "");
      } else if(this.form.value.password != this.form.value.confirmPassword) {
        this.notifications.showInfo("The password confirmation does not match", "");
      } else {
        this.profileService.register(this.register).subscribe(result => {
          if(result.success) {
            this.profileService.setToken(result.token);
            this.router.navigateByUrl('menu');
          } else {
            this.notifications.showError(result.data, "");
          }
        },
        error => {
          console.log(error);
        });
      }
    } else {
      if (this.form.value.name.length == 0 || this.form.value.state.length == 0
        ||  this.form.value.city.length == 0 ||  this.form.value.streetAddress.length == 0
        || this.form.value.email.length == 0 || this.form.value.confirmEmail.length == 0
        || this.form.value.password.length == 0 || this.form.value.confirmPassword.length == 0) {
        this.notifications.showInfo("Fill all spaces", "");
      } else if(!Validators.email(this.form.value.email) || !Validators.email(this.form.value.confirmEmail)) {
        this.notifications.showInfo("Insert a valid email", "");
      } else if(this.form.value.email != this.form.value.confirmEmail) {
        this.notifications.showInfo("The email confirmation does not match", "");
      } else {
        this.notifications.showInfo("Verify the information entered ", "");
      }
    }

    
  }
}
