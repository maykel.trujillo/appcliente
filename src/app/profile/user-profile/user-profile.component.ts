import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from 'src/app/interfaces/profile.interface';
import { IconsService } from 'src/app/services/icons.service';
import { NotificationService } from 'src/app/services/notifications.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: [ './user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

 profile:Profile = {
  Name:"",
  City:"",
  Id:"",
  State:"",
  StreetAdress:"",
  Email:"",
 };

 tempProfile:Profile = {
  Name:"",
  City:"",
  Id:"",
  State:"",
  StreetAdress:"",
  Email:"",
 };


  constructor(public profileService:ProfileService,
    public icons:IconsService, 
    private notifications:NotificationService,
    private router:Router) { 
  }

  ngOnInit(): void {
    this.profileService.getUserProfile().subscribe(result => {
      if(result.success) {
        this.profile = {
          Name:result.data.name,
          City:result.data.city,
          Id:result.data.id,
          State:result.data.state,
          StreetAdress:result.data.streetAdress,
          Email:result.data.email
        };
        this.tempProfile = {
          Name:result.data.name,
          City:result.data.city,
          Id:result.data.id,
          State:result.data.state,
          StreetAdress:result.data.streetAdress,
          Email:result.data.email
        };
      }
    });
  }

  initEditProfile() {
    this.profileService.setEditingProfileCondition("true");
  }

  cancelEditProfile() {
    this.profileService.setEditingProfileCondition("false");
  }

  saveChanges() {
    if(this.tempProfile.Name != this.profile.Name || this.tempProfile.State != this.profile.State
      || this.tempProfile.StreetAdress != this.profile.StreetAdress || this.tempProfile.Email != this.profile.Email
      || this.tempProfile.City != this.profile.City) {
        
      this.profileService.editProfile(this.profile).subscribe(result => {
        console.log(result);
        if(result.success) {
          this.notifications.showSuccess(result.data, "");
          this.cancelEditProfile();
        } else {
          this.notifications.showInfo(result.data, "");
        }
      });
    } else {
      this.cancelEditProfile();
    }
  }

  logout() {
    this.profileService.logout();
    this.router.navigateByUrl('');
  }

}
