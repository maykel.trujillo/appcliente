import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/interfaces/profile.interface';
import { NotificationService } from 'src/app/services/notifications.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
  ]
})
export class LoginComponent {

  login:Login = {
    Email: "",
    PasswordHash: ""
  }

  form:FormGroup;

  constructor(private profileService:ProfileService,
    private router:Router,
    private notifications:NotificationService,
    private formBuilder:FormBuilder) { 
      this.form = this.formBuilder.group({
      });
    }

  initLogin() {
    this.form = this.formBuilder.group({
      'email': [this.login.Email, [Validators.required, Validators.email]],
      'password': [this.login.PasswordHash, [Validators.required]],
    });

    if(this.form.valid) {
      this.profileService.login(this.login).subscribe( result => {
        if(result.success) {
          this.profileService.setToken(result.token);
          this.router.navigateByUrl('menu');
        } else {
          this.notifications.showError(result.data, "");
        }
      });
    } else {
      if (this.form.value.email.length == 0) {
        this.notifications.showInfo("Insert an email", "");
      } else if (this.form.value.password.length == 0) {
        this.notifications.showInfo("Insert a password", "");
      } else if(!Validators.email(this.form.value.email)) {
        this.notifications.showInfo("Insert a valid email", "");
      }
    }
  }
}
