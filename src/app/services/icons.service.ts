import { Injectable } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { faBan } from '@fortawesome/free-solid-svg-icons';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';


@Injectable({
  providedIn: 'root'
})
export class IconsService {

  constructor() { }

  get user() {
    return faUser;
  }

  get list() {
    return faBars;
  }

  get shoppingCart() {
    return faShoppingCart;
  }

  get edit() {
    return faUserEdit;
  }

  get cancel() {
    return faBan;
  }

  get next() {
    return faChevronRight;
  }

  get back() {
    return faChevronLeft;
  }

  get remove() {
    return faTrash;
  }

  get leave() {
    return faSignOutAlt;
  }
}
