import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { ProfileService } from './profile.service';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Dish, DishToOrder } from '../interfaces/dishes.interface';

@Injectable({
    providedIn: 'root'
  })
export class DishesService{

    url:string = "https://localhost:44309/API/Customer/";

    constructor(private profileService:ProfileService, private http:HttpClient, ) {
    }

    getMenu(filterName:string, minPrice:number, maxPrice:number): Observable<any> {
      const params = new HttpParams().set('filter', filterName)
        .set('minPrice', minPrice)
        .set('maxPrice', maxPrice);
      return this.http.get(this.url, {params});
    }

    getDishInformation(id:number): Observable<any> {
      const params = new HttpParams().set('id', id);
      return this.http.get(this.url + "GetDishDescription?", {params});
    }

    getImage(imageUrl: string): Observable<Blob> {
      const params = new HttpParams().set('url', imageUrl);
      return this.http.get(this.url + "GetDishImage?", { responseType: 'blob' , params});
    }

    confirmOrder(dishToOrder:DishToOrder[]): Observable<any> {

      const params = new HttpParams().set('dishes', JSON.stringify(dishToOrder))
        .set('customerId', this.profileService.getUserLogged());
      return this.http.post(this.url + "SaveOrder?", params);
    }
}