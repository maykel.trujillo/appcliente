import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Login, Profile, Register } from '../interfaces/profile.interface';
import { Observable } from "rxjs";
import { CookieService } from "ngx-cookie-service";

@Injectable({providedIn: "root"})
export class ProfileService {
    constructor(private http:HttpClient, private cookies:CookieService) {
    }

    url:string = "https://localhost:44309/API/Customer/";
    
    login(user: Login): Observable<any> {
      const params = new HttpParams().set('profile', JSON.stringify(user));
      return this.http.post(this.url + "Login?", params);
    }

    register(user: Register): Observable<any> {
      const params = new HttpParams().set('profile', JSON.stringify(user));
      return this.http.post(this.url + "Registration?", params);
    }

    editProfile(user:Profile): Observable<any> {
      const params = new HttpParams().set('profile', JSON.stringify(user));
      return this.http.post(this.url + "UpdateProfile?", params);
    }

    setToken(token:string) {
      this.cookies.set("token", token);
    }

    getUserLogged() {
      return this.cookies.get("token");
    }

    logout() {
      this.cookies.delete("token");
    }

    getUserProfile(): Observable<any> {
      const params = new HttpParams().set('customerId', this.getUserLogged());
      return this.http.get(this.url + "GetProfile?", {params});
    }

    
    setEditingProfileCondition(condition:string) {
      this.cookies.set("editingProfile", condition);
    }

    get editingProfileCondition() {
      return this.cookies.get("editingProfile") == "true" ? true : false;
    }

    get getLoginCondition() {
        return this.getUserLogged() != "" ? true : false;
    }

    setActualPositionBar(position:string) {
        this.cookies.set("position", position);
    }

    get actualPositionBar() {
        return this.cookies.get("position");
    }
}