import { Injectable } from "@angular/core";
import { Dexie } from "dexie";
import { Dish, DishToOrder } from '../interfaces/dishes.interface';

@Injectable({
    providedIn: 'root'
  })
export class OrderService extends Dexie {
    dishes: Dexie.Table<DishToOrder, number>;

    private dishToOrder:DishToOrder = {
        id:0,
        amount:0,
        name:"",
        totalPrice:0
    };
    
    constructor() {
      super('order');
      this.version(15).stores({
        dishes: 'id,name,totalPrice,amount'
      });
      this.dishes = this.table("dishes");
    }

    addDishToBasket(dish:Dish) {
        this.dishToOrder = {
            id: dish.id,
            name: dish.name,
            amount:dish.amount,
            totalPrice:dish.totalPrice
        };
        return this.transaction('rw', this.dishes, async () => {
            this.dishes.put(this.dishToOrder);
        });
    }

    deleteDishFromBasket(id:number) {
        return this.transaction('rw', this.dishes, async () => {
            this.dishes.delete(id);
        });
    }

    deleteOrder() {
        return this.transaction('rw', this.dishes, async () => {
            this.dishes.clear();
        });
    }

    async getOrder() {
        return Promise.all(await this.dishes.toArray());
    }
}