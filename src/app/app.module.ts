import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BarModule } from './bar/bar.module';
import { ProfileModule } from './profile/profile.module';
import { AppRoutingModule } from './app.routing.module';
import { CookieService } from 'ngx-cookie-service';
import { DishesModule } from './dishes/dishes.module';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ToastNoAnimationModule.forRoot(),
    FontAwesomeModule,
    BarModule,
    ProfileModule,
    AppRoutingModule,
    DishesModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
