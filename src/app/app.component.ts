import { Component } from '@angular/core';
import { ProfileService } from './services/profile.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent {
  
  constructor(public profileService: ProfileService) {
    }
  
  title = 'Customer';
}