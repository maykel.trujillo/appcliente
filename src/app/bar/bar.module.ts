import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [
    NavBarComponent
  ],
  imports: [
    CommonModule, 
    RouterModule,
    FontAwesomeModule
  ],
  exports: [
    NavBarComponent
  ]
})
export class BarModule { }
