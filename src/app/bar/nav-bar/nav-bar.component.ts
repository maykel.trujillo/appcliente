import { Component, OnInit } from '@angular/core';
import { IconsService } from 'src/app/services/icons.service';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: [],
  styleUrls: [
    './nav-bar.component.css'
  ]
})
export class NavBarComponent implements OnInit {


  constructor(public icons:IconsService, 
    public profile:ProfileService) { 
  }

  ngOnInit(): void {
  }
}
